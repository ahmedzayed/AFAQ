﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.Core.VM
{
  public  class ImageVM
    {
        public int ImageID { get; set; }
        public string Name { get; set; }
        public string photo { get; set; }
    }
}
