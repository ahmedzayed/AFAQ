﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.Core.VM
{
public class Memory_Pattern_AnswersFM
    {
        public int ID { get; set; }
        public string Answer { get; set; }
        public Nullable<int> Answer_MeanID { get; set; }
        public Nullable<int> QustionID { get; set; }

    }
}
