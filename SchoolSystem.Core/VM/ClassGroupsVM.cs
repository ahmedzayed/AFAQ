﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.Core.VM
{
  public  class ClassGroupsVM
    {
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public Nullable<System.DateTime> CreationDate { get; set; }
        public string ImageUrl { get; set; }
    }
}
