﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.Core.VM
{
  public  class AgentsVM
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string ENName { get; set; }
        public string ENAddress { get; set; }
        public string facebook { get; set; }
        public string Url_agent { get; set; }
    }
}
