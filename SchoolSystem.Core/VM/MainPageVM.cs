﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.Core.VM
{
   public class MainPageVM
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string PhotoPath { get; set; }
        public string type { get; set; }
        public string Brief { get; set; }
        public bool ISActive { get; set; }
    }
}
