﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.Core.VM
{
  public  class CountersVM
    {
        public int CounterID { get; set; }
        public string Title { get; set; }
        public int Numbers { get; set; }
        public bool Activate { get; set; }
        public string TitleEN { get; set; }
    }
}
