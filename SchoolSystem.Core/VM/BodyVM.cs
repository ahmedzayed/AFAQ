﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.Core.VM
{
  public  class BodyVM
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public bool visable { get; set; }
        public bool main { get; set; }
        public string type { get; set; }
    }
}
