﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.Core.VM
{
   public class Memory_pattern_AnswerMeanVM
    {
        public int ID { get; set; }
        public string AnswerMean { get; set; }
        public string AnswerMeanDetails { get; set; }
        public string AnswerMeanAdvices { get; set; }
    }
}
