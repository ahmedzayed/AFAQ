﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.Core.VM
{
  public  class Memory_Pattern_AnswersVM
    {
        public int ID { get; set; }
        public string Answer { get; set; }
        public string Answer_MeanName { get; set; }
        public string QustionName { get; set; }
    }
}
