﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.Core.VM
{
  public  class GameVM
    {
        public int GameId { get; set; }
        public string GameURL { get; set; }
        public string GameImage { get; set; }
        public string EngName { get; set; }
        public string ARName { get; set; }
    }
}
