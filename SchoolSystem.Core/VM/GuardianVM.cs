﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.Core.VM
{
   public class GuardianVM
    {
        public int Guard_ID { get; set; }
        public string Guard_FullName { get; set; }
        public string Guard_Phone { get; set; }
        public string Guard_Job { get; set; }
        public string Guard_Gender { get; set; }
        public string Std_relation { get; set; }
        public string Guard_Mail { get; set; }
        public string Guard_HomePhone { get; set; }
        public string Guard_JobPhone { get; set; }
        public string photo { get; set; }
    }
}
