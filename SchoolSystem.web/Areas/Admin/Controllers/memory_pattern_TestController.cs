﻿using SchoolSystem.Core;
using SchoolSystem.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSystem.web.Areas.Admin.Controllers
{
    public class memory_pattern_TestController : Controller
    {
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllmemory_pattern_Test".ExecuParamsSqlOrStored(false).AsList<memory_pattern_TestVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            memory_pattern_TestVM Obj = new memory_pattern_TestVM();
            return PartialView("~/Areas/Admin/Views/memory_pattern_Test/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(memory_pattern_TestVM model)
        {
            model.CreateDate = DateTime.Now;
            var data = "SP_Addmemory_pattern_Test".ExecuParamsSqlOrStored(false, "Title".KVP(model.Title), "IsAvilable".KVP(model.IsAvilable)
                , "CreateDate".KVP(model.CreateDate), "ID".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_Selectmemory_pattern_TestById".ExecuParamsSqlOrStored(false, "ID".KVP(id)).AsList<memory_pattern_TestVM>();
            if (model == null)
            {
                return HttpNotFound();
            }

            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            memory_pattern_TestVM obj = new memory_pattern_TestVM();
            obj.ID = id;
            return PartialView("~/Areas/Admin/Views/memory_pattern_Test/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_Deletememory_pattern_Test".ExecuParamsSqlOrStored(false, "ID".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}