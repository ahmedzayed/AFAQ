﻿using SchoolSystem.Core;
using SchoolSystem.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSystem.web.Areas.Admin.Controllers
{
    public class Memory_Pattern_AnswersController : Controller
    {
        // GET: Admin/Memory_Pattern_Answers
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllMemory_Pattern_Answers".ExecuParamsSqlOrStored(false).AsList<Memory_Pattern_AnswersVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            Memory_Pattern_AnswersFM Obj = new Memory_Pattern_AnswersFM();
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("Sp_CatMemorymeanDrop".ExecuParamsSqlOrStored(false).AsList<Memory_pattern_AnswerMeanDrp>().Select(s => new SelectListItem
            {
                Text = s.AnswerMean,
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.ListMemorymean = lst;
            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_Memory_pattern_QuestionsDrop".ExecuParamsSqlOrStored(false).AsList<Memory_pattern_QuestionsDrp>().Select(s => new SelectListItem
            {
                Text = s.Qustion,
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.ListTestCat = lst2;
            return PartialView("~/Areas/Admin/Views/Memory_Pattern_Answers/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(Memory_Pattern_AnswersFM model)
        {

            var data = "SP_AddMemory_Pattern_Answers".ExecuParamsSqlOrStored(false, "Answer".KVP(model.Answer), "Answer_MeanID".KVP(model.Answer_MeanID), "QustionID".KVP(model.QustionID)
                 , "ID".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectMemory_Pattern_AnswersById".ExecuParamsSqlOrStored(false, "ID".KVP(id)).AsList<Memory_Pattern_AnswersFM>();
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("Sp_CatMemorymeanDrop".ExecuParamsSqlOrStored(false).AsList<Memory_pattern_AnswerMeanDrp>().Select(s => new SelectListItem
            {
                Text = s.AnswerMean,
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.ListMemorymean = lst;
            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_Memory_pattern_QuestionsDrop".ExecuParamsSqlOrStored(false).AsList<Memory_pattern_QuestionsDrp>().Select(s => new SelectListItem
            {
                Text = s.Qustion,
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.ListTestCat = lst2;
            if (model == null)
            {
                return HttpNotFound();
            }

            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            TestVM obj = new TestVM();
            obj.ID = id;
            return PartialView("~/Areas/Admin/Views/Test/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteMemory_Pattern_Answers".ExecuParamsSqlOrStored(false, "ID".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}