﻿using SchoolSystem.Core;
using SchoolSystem.Core.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSystem.web.Areas.Admin.Controllers
{
    public class MCareersController : Controller
    {
        // GET: Admin/MCareers
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllCareers".ExecuParamsSqlOrStored(false).AsList<MainPageVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            MainPageVM Obj = new MainPageVM();
            return PartialView("~/Areas/Admin/Views/MCareers/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(MainPageVM model)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.PhotoPath = "Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }



            }

            var data = "SP_AddMainPage".ExecuParamsSqlOrStored(false, "Title".KVP(model.Title), "Description".KVP(model.Description),
                "PhotoPath".KVP(model.PhotoPath), "type".KVP("Careers"), "Brief".KVP(model.Brief), "IsActive".KVP(model.ISActive), "ID".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectMainPageById".ExecuParamsSqlOrStored(false, "ID".KVP(id)).AsList<MainPageVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/Admin/Views/MCareers/Create.cshtml", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            MainPageVM obj = new MainPageVM();
            obj.ID = id;
            return PartialView("~/Areas/Admin/Views/MCareers/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteMainPage".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}
