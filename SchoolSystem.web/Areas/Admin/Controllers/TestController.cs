﻿using SchoolSystem.Core;
using SchoolSystem.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSystem.web.Areas.Admin.Controllers
{
    public class TestController : Controller
    {
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllTest".ExecuParamsSqlOrStored(false).AsList<TestVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            TestVM Obj = new TestVM();
            return PartialView("~/Areas/Admin/Views/Test/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(TestVM model)
        {
            var data = "SP_AddTest".ExecuParamsSqlOrStored(false, "Title".KVP(model.Title), "IsAvilable".KVP(model.IsAvilable)
                , "CreateDate".KVP(model.CreateDate), "ID".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectTestById".ExecuParamsSqlOrStored(false, "ID".KVP(id)).AsList<TestVM>();
            if (model == null)
            {
                return HttpNotFound();
            }

            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            TestVM obj = new TestVM();
            obj.ID = id;
            return PartialView("~/Areas/Admin/Views/Test/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteTest".ExecuParamsSqlOrStored(false, "ID".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}