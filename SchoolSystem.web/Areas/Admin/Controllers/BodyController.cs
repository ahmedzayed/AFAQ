﻿using SchoolSystem.Core;
using SchoolSystem.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSystem.web.Areas.Admin.Controllers
{
    public class BodyController : Controller
    {
        // GET: Admin/Body
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllBody".ExecuParamsSqlOrStored(false).AsList<BodyVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            BodyVM Obj = new BodyVM();
            return PartialView("~/Areas/Admin/Views/Body/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(BodyVM model)
        {
          

            var data = "SP_AddBody".ExecuParamsSqlOrStored(false, "Title".KVP(model.Title), "Details".KVP(model.Details),
                "visable".KVP(model.visable), "main".KVP(model.main), "type".KVP(model.type), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectBodyById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<BodyVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            BodyVM obj = new BodyVM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/Body/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteBody".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}