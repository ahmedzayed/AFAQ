﻿using SchoolSystem.Core;
using SchoolSystem.Core.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSystem.web.Areas.Admin.Controllers
{
    public class ClassGroupsController : Controller
    {
        // GET: Admin/ClassGroups
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllClassGroup".ExecuParamsSqlOrStored(false).AsList<ClassGroupsVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            ClassGroupsVM Obj = new ClassGroupsVM();
            return PartialView("~/Areas/Admin/Views/ClassGroups/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(ClassGroupsVM model)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.ImageUrl = "Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }



            }

            var data = "SP_AddClassGroups".ExecuParamsSqlOrStored(false, "GroupName".KVP(model.GroupName), "ImageUrl".KVP(model.ImageUrl),
                "CreationDate".KVP(DateTime.Now), "GroupID".KVP(model.GroupID)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectClassGroupsById".ExecuParamsSqlOrStored(false, "GroupID".KVP(id)).AsList<ClassGroupsVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/Admin/Views/ClassGroups/Create.cshtml", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            ClassGroupsVM obj = new ClassGroupsVM();
            obj.GroupID = id;
            return PartialView("~/Areas/Admin/Views/ClassGroups/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteClassGroups".ExecuParamsSqlOrStored(false, "GroupID".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}
