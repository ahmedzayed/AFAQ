﻿using SchoolSystem.Core;
using SchoolSystem.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSystem.web.Areas.Admin.Controllers
{
    public class Memory_pattern_AnswerMeanController : Controller
    {
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllMemory_pattern_AnswerMean".ExecuParamsSqlOrStored(false).AsList<Memory_pattern_AnswerMeanVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            Memory_pattern_AnswerMeanVM Obj = new Memory_pattern_AnswerMeanVM();
            return PartialView("~/Areas/Admin/Views/Memory_pattern_AnswerMean/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(Memory_pattern_AnswerMeanVM model)
        {

            var data = "SP_AddMemory_pattern_AnswerMean".ExecuParamsSqlOrStored(false, "AnswerMean".KVP(model.AnswerMean), "AnswerMeanDetails".KVP(model.AnswerMeanDetails)
                , "AnswerMeanAdvices".KVP(model.AnswerMeanAdvices),"ID".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectMemory_pattern_AnswerMeanById".ExecuParamsSqlOrStored(false, "ID".KVP(id)).AsList<Memory_pattern_AnswerMeanVM>();
            if (model == null)
            {
                return HttpNotFound();
            }

            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            Memory_pattern_AnswerMeanVM obj = new Memory_pattern_AnswerMeanVM();
            obj.ID = id;
            return PartialView("~/Areas/Admin/Views/Memory_pattern_AnswerMean/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteMemory_pattern_AnswerMean".ExecuParamsSqlOrStored(false, "ID".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}