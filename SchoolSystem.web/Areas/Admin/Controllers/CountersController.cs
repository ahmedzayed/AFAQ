﻿using SchoolSystem.Core;
using SchoolSystem.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSystem.web.Areas.Admin.Controllers
{
    public class CountersController : Controller
    {

        // GET: Admin/Counters
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllCounters".ExecuParamsSqlOrStored(false).AsList<CountersVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            CountersVM Obj = new CountersVM();
            return PartialView("~/Areas/Admin/Views/Counters/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(CountersVM model)
        {
            
            var data = "SP_AddCounters".ExecuParamsSqlOrStored(false, "Title".KVP(model.Title), "TitleEN".KVP(model.TitleEN),
                "Numbers".KVP(model.Numbers), "Activate".KVP(model.Activate), "CounterID".KVP(model.CounterID)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectCountersById".ExecuParamsSqlOrStored(false, "CounterID".KVP(id)).AsList<CountersVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            CountersVM obj = new CountersVM();
            obj.CounterID = id;
            return PartialView("~/Areas/Admin/Views/Counters/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteCounters".ExecuParamsSqlOrStored(false, "CounterID".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}