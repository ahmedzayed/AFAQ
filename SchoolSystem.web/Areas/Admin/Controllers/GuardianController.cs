﻿using Microsoft.AspNet.Identity.Owin;
using SchoolSystem.Core;
using SchoolSystem.Core.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSystem.web.Areas.Admin.Controllers
{
    public class GuardianController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;


        public GuardianController()
        {
        }

        public GuardianController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllGuardian".ExecuParamsSqlOrStored(false).AsList<GuardianVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            GuardianFM Obj = new GuardianFM();
            return PartialView("~/Areas/Admin/Views/Guardian/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(GuardianFM model)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.photo = "Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }



            }



            var data = "SP_AddGuardian".ExecuParamsSqlOrStored(false, "Guard_FullName".KVP(model.Guard_FullName),
                "Guard_HomePhone".KVP(model.Guard_HomePhone), "Guard_Job".KVP(model.Guard_Job),
                "Guard_Mail".KVP(model.Guard_Mail), "Guard_Phone".KVP(model.Guard_Phone), "Std_relation".KVP(model.Std_relation),
                "photo".KVP(model.photo),
                "Guard_ID".KVP(model.Guard_ID)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectGuardianById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<GuardianVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            GuardianVM obj = new GuardianVM();
            obj.Guard_ID = id;
            return PartialView("~/Areas/Admin/Views/Guardian/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteGuardian".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}
