﻿using SchoolSystem.Core;
using SchoolSystem.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSystem.web.Areas.Admin.Controllers
{
    public class AgentsController : Controller
    {
        // GET: Admin/Agents
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllAgents".ExecuParamsSqlOrStored(false).AsList<AgentsVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            AgentsVM Obj = new AgentsVM();
            return PartialView("~/Areas/Admin/Views/Agents/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(AgentsVM model)
        {


            var data = "SP_AddAgents".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name), "ENName".KVP(model.ENName),
                "Phone".KVP(model.Phone), "Address".KVP(model.Address), "facebook".KVP(model.facebook), "Url_agent".KVP(model.Url_agent), "ID".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectAgentsById".ExecuParamsSqlOrStored(false, "ID".KVP(id)).AsList<AgentsVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            AgentsVM obj = new AgentsVM();
            obj.ID = id;
            return PartialView("~/Areas/Admin/Views/Agents/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteAgents".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}