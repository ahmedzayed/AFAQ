﻿using SchoolSystem.Core;
using SchoolSystem.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSystem.web.Areas.Admin.Controllers
{
    public class CapabilitiesqustionsController : Controller
    {
        // GET: Admin/Capabilitiesqustions
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllCapabilitiesqustions".ExecuParamsSqlOrStored(false).AsList<CapabilitiesqustionsVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            CapabilitiesqustionsFM Obj = new CapabilitiesqustionsFM();
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("Sp_CatTestDrop".ExecuParamsSqlOrStored(false).AsList<Test_capabilities_CategoryDR>().Select(s => new SelectListItem
            {
                Text = s.TestCategory,
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.ListTestCat = lst;
            return PartialView("~/Areas/Admin/Views/Capabilitiesqustions/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(CapabilitiesqustionsFM model)
        {

            var data = "SP_AddCapabilitiesqustions".ExecuParamsSqlOrStored(false, "Question".KVP(model.Question), "QustionCategoryID".KVP(model.QustionCategoryID)
                 ,"ID".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectCapabilitiesqustionsById".ExecuParamsSqlOrStored(false, "ID".KVP(id)).AsList<CapabilitiesqustionsFM>();
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("Sp_CatTestDrop".ExecuParamsSqlOrStored(false).AsList<Test_capabilities_CategoryDR>().Select(s => new SelectListItem
            {
                Text = s.TestCategory,
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.ListTestCat = lst;
            if (model == null)
            {
                return HttpNotFound();
            }

            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            TestVM obj = new TestVM();
            obj.ID = id;
            return PartialView("~/Areas/Admin/Views/Test/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteCapabilitiesqustions".ExecuParamsSqlOrStored(false, "ID".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}