﻿using SchoolSystem.Core;
using SchoolSystem.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSystem.web.Areas.Admin.Controllers
{
    public class SocialMediaController : Controller
    {

        // GET: Admin/SocialMedia
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllSocialMedia".ExecuParamsSqlOrStored(false).AsList<SocialMediaVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            SocialMediaVM Obj = new SocialMediaVM();
            return PartialView("~/Areas/Admin/Views/SocialMedia/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(SocialMediaVM model)
        {
            
            var data = "SP_AddSocialMedia".ExecuParamsSqlOrStored(false, "facebook".KVP(model.facebook), "twitter".KVP(model.twitter),
                "Instgram".KVP(model.Instgram), "LinkedIn".KVP(model.LinkedIn),"xx".KVP(model.xx), "SocialID".KVP(model.SocialID)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectSocialMediaById".ExecuParamsSqlOrStored(false, "SocialID".KVP(id)).AsList<SocialMediaVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            SocialMediaVM obj = new SocialMediaVM();
            obj.SocialID = id;
            return PartialView("~/Areas/Admin/Views/SocialMedia/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteSocialMedia".ExecuParamsSqlOrStored(false, "SocialID".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}