﻿using SchoolSystem.Core;
using SchoolSystem.Core.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSystem.web.Areas.Admin.Controllers
{
    public class ImageController : Controller
    {
        // GET: Admin/Image
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllImage".ExecuParamsSqlOrStored(false).AsList<ImageVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            ImageVM Obj = new ImageVM();
            return PartialView("~/Areas/Admin/Views/Image/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(ImageVM model)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.photo = "Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }



            }

            var data = "SP_AddImage".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name), "photo".KVP(model.photo),
               "ImageID".KVP(model.ImageID)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectImageById".ExecuParamsSqlOrStored(false, "ID".KVP(id)).AsList<ImageVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            ImageVM obj = new ImageVM();
            obj.ImageID = id;
            return PartialView("~/Areas/Admin/Views/Image/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteImage".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}