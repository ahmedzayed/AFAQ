﻿using SchoolSystem.Core;
using SchoolSystem.Core.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSystem.web.Areas.Admin.Controllers
{
    public class GameController : Controller
    {
        // GET: Admin/Game
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllGame".ExecuParamsSqlOrStored(false).AsList<GameVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            GameVM Obj = new GameVM();

            
            return PartialView("~/Areas/Admin/Views/Game/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(GameVM model)
        {

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.GameImage = "Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }




            }
            model.EngName = model.ARName;
            var data = "SP_AddGame".ExecuParamsSqlOrStored(false, "ARName".KVP(model.ARName),
                "EngName".KVP(model.EngName), "GameImage".KVP(model.GameImage), "GameURL".KVP(model.GameURL)
                 , "ID".KVP(model.GameId)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectGameById".ExecuParamsSqlOrStored(false, "ID".KVP(id)).AsList<GameVM>();
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
           
            if (model == null)
            {
                return HttpNotFound();
            }

            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            TestVM obj = new TestVM();
            obj.ID = id;
            return PartialView("~/Areas/Admin/Views/Test/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteGame".ExecuParamsSqlOrStored(false, "ID".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}