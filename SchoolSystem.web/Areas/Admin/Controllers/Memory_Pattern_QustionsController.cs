﻿using SchoolSystem.Core;
using SchoolSystem.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSystem.web.Areas.Admin.Controllers
{
    public class Memory_Pattern_QustionsController : Controller
    {
        // GET: Admin/Memory_Pattern_Qustions
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllMemory_Pattern_Qustions".ExecuParamsSqlOrStored(false).AsList<Memory_Pattern_QustionsVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            Memory_Pattern_QustionsVM Obj = new Memory_Pattern_QustionsVM();
            return PartialView("~/Areas/Admin/Views/Memory_Pattern_Qustions/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(Memory_Pattern_QustionsVM model)
        {

            var data = "SP_AddMemory_Pattern_Qustions".ExecuParamsSqlOrStored(false, "Qustion".KVP(model.Qustion),  "ID".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectMemory_Pattern_QustionsById".ExecuParamsSqlOrStored(false, "ID".KVP(id)).AsList<Memory_Pattern_QustionsVM>();
            if (model == null)
            {
                return HttpNotFound();
            }

            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            Memory_Pattern_QustionsVM obj = new Memory_Pattern_QustionsVM();
            obj.ID = id;
            return PartialView("~/Areas/Admin/Views/Memory_Pattern_Qustions/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteMemory_Pattern_Qustions".ExecuParamsSqlOrStored(false, "ID".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}