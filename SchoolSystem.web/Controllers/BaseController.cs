﻿using SchoolSystem.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSystem.web.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
      
        public ActionResult ToggleLanguage(string returnUrl)
        {
            Sessions.CurrentCulture = Sessions.CurrentCulture == "ar-EG" ? "en-CA" : "ar-EG";
            if (Sessions.CurrentCulture == "en-CA")
            {
                Sessions.CurrentText = "العربية";
            }
            else if (Sessions.CurrentCulture == "ar-EG")
            {
                Sessions.CurrentText = "English";

            }

            return Redirect(returnUrl);
        }
    }
}